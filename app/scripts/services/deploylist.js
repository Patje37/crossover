'use strict';

/**
 * @ngdoc service
 * @name crossoverApp.deployList
 * @description
 * # deployList
 * Service in the crossoverApp.
 */
angular.module('crossoverApp')
  .factory('deployList', ['$http', '$q', function ($http, $q) {
    var cachedValues = [];
    return {
      getList: function() {
        return cachedValues.length ?
          $q.resolve(cachedValues) :
          $http.get('/list.json', {
            responseType: 'json'
          }).then(function(response) {
            cachedValues = response.data;
            return cachedValues;
          });
      }
    }
  }]);
