'use strict';

/**
 * @ngdoc overview
 * @name crossoverApp
 * @description
 * # crossoverApp
 *
 * Main module of the application.
 */
angular
  .module('crossoverApp', [
    'ngAnimate',
    'ngRoute'
  ])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
